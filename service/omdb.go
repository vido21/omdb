package omdbservice

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

const API_KEY = "faf7e5bb"

type OmdbService struct {
}

func (c *OmdbService) GetMovieByID(param GetMovieParam) (result GetMovieResult) {
	url := fmt.Sprintf("https://www.omdbapi.com/?apikey=%s&i=%s", API_KEY, param.ID)

	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Error making the HTTP request:", err)
		return GetMovieResult{}
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		fmt.Println("OMDB API returned an error:", resp.Status)
		return GetMovieResult{}
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading the response:", err)
		return GetMovieResult{}
	}

	var response GetMovieResponse
	fmt.Println(body)
	if err := json.Unmarshal(body, &response); err != nil {
		fmt.Println("Error decoding the JSON response:", err)
		return GetMovieResult{}
	}

	fmt.Println(response)

	actors := strings.Split(response.Actors, ", ")

	return GetMovieResult{
		ID:        param.ID,
		Title:     response.Title,
		Rated:     response.Rated,
		Plot:      response.Plot,
		Type:      response.Type,
		Year:      response.Year,
		Genre:     response.Genre,
		Director:  response.Director,
		Actors:    actors,
		Language:  response.Language,
		Country:   response.Country,
		PosterURL: response.PosterURL,
	}
}

func (c *OmdbService) SearchMovies(param SearchMoviesParam) (result SearchMoviesResult) {
	url := fmt.Sprintf("http://www.omdbapi.com/?apikey=%s&s=%s&page=%d", API_KEY, param.Query, param.Page)

	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Error making the HTTP request:", err)
		return SearchMoviesResult{}
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		fmt.Println("OMDB API returned an error:", resp.Status)
		return SearchMoviesResult{}
	}

	fmt.Println(resp)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading the response:", err)
		return SearchMoviesResult{}
	}

	var response SearchMoviesResponse
	if err := json.Unmarshal(body, &response); err != nil {
		fmt.Println("Error decoding the JSON response:", err)
		return SearchMoviesResult{}
	}

	fmt.Println(response)

	movieResult := []MovieResult{}

	var totalResult uint64 = 0

	for _, movie := range response.Movies {
		if movie.Type != param.Type {
			continue
		}
		movieResult = append(movieResult, MovieResult(movie))
		totalResult += 1
	}

	return SearchMoviesResult{
		MovieResult:  movieResult,
		TotalResults: totalResult,
	}
}
