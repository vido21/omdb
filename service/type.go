package omdbservice

type GetMovieParam struct {
	ID string `json:"id"`
}

type GetMovieResult struct {
	ID        string   `json:"id"`
	Title     string   `json:"title"`
	Year      string   `json:"year"`
	Rated     string   `json:"rated"`
	Genre     string   `json:"genre"`
	Plot      string   `json:"plot"`
	Director  string   `json:"director"`
	Actors    []string `json:"actors"`
	Language  string   `json:"language"`
	Country   string   `json:"country"`
	Type      string   `json:"type"`
	PosterURL string   `json:"poster"`
}

type GetMovieResponse struct {
	Title     string `json:"Title"`
	Year      string `json:"Year"`
	Rated     string `json:"Rated"`
	Genre     string `json:"Genre"`
	Plot      string `json:"Plot"`
	Director  string `json:"Director"`
	Actors    string `json:"Actors"`
	Language  string `json:"Language"`
	Country   string `json:"Country"`
	Type      string `json:"Type"`
	PosterURL string `json:"Poster"`
}

type SearchMoviesParam struct {
	Query string `json:"query"`
	Type  string `json:"type"`
	Page  uint64 `json:"page"`
}

type SearchMoviesResponse struct {
	Movies       []Movie `json:"Search"`
	TotalResults string  `json:"totalResults"`
	Response     string  `json:"Response"`
}

type Movie struct {
	ID     string `json:"imdbID"`
	Title  string `json:"Title"`
	Year   string `json:"Year"`
	Type   string `json:"Type"`
	Poster string `json:"Poster"`
}

type SearchMoviesResult struct {
	MovieResult  []MovieResult `json:"Search"`
	TotalResults uint64        `json:"totalResults"`
}

type MovieResult struct {
	ID     string `json:"id"`
	Title  string `json:"Title"`
	Year   string `json:"Year"`
	Type   string `json:"Type"`
	Poster string `json:"Poster"`
}
