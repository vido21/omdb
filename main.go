package main

import (
	"context"
	"fmt"
	"net"

	parse "gitlab.com/vido21/omdb/parse"
	omdb "gitlab.com/vido21/omdb/proto/contract"
	omdbService "gitlab.com/vido21/omdb/service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type server struct{}

func (s *server) GetMovieByID(ctx context.Context, req *omdb.GetMovieByIDRequest) (*omdb.GetMovieByIDResponse, error) {
	if req.GetId() == "" {
		err := status.Errorf(codes.InvalidArgument, "Missing or empty 'id' parameter")
		return nil, err
	}

	service := omdbService.OmdbService{}

	result := service.GetMovieByID(omdbService.GetMovieParam{
		ID: req.GetId(),
	})

	response := parse.ParseGetMovieByIDResult(result)

	return response, nil
}

func (s *server) SearchMovies(ctx context.Context, req *omdb.SearchMoviesRequest) (*omdb.SearchMoviesResponse, error) {

	if req.GetQuery() == "" || req.GetType() == "" {
		err := status.Errorf(codes.InvalidArgument, "Missing parameters")
		return nil, err
	}

	service := omdbService.OmdbService{}

	result := service.SearchMovies(omdbService.SearchMoviesParam{
		Query: req.GetQuery(),
		Type:  req.GetType(),
		Page:  req.GetPage(),
	})

	response := parse.ParseSearchMoviesResult(result)
	return response, nil
}

func main() {
	listener, err := net.Listen("tcp", ":50051")
	if err != nil {
		panic(err)
	}

	srv := grpc.NewServer()
	omdb.RegisterOMDBServiceServer(srv, &server{})

	fmt.Println("Server started on port :50051")
	if err := srv.Serve(listener); err != nil {
		panic(err)
	}
}
