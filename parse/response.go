package parse

import (
	omdb "gitlab.com/vido21/omdb/proto/contract"
	omdbService "gitlab.com/vido21/omdb/service"
)

func ParseGetMovieByIDResult(result omdbService.GetMovieResult) *omdb.GetMovieByIDResponse {
	return &omdb.GetMovieByIDResponse{
		Id:        result.ID,
		Title:     result.Title,
		Plot:      result.Plot,
		Type:      result.Type,
		Rated:     result.Rated,
		Year:      result.Year,
		Genre:     result.Genre,
		Director:  result.Director,
		Actors:    result.Actors,
		Language:  result.Language,
		Country:   result.Country,
		PosterUrl: result.PosterURL,
	}
}

func ParseSearchMoviesResult(result omdbService.SearchMoviesResult) *omdb.SearchMoviesResponse {
	movies := []*omdb.MovieResult{}

	for _, movie := range result.MovieResult {
		movies = append(movies, &omdb.MovieResult{
			Id:        movie.ID,
			Title:     movie.Title,
			Type:      movie.Type,
			Year:      movie.Year,
			PosterUrl: movie.Poster,
		})
	}

	return &omdb.SearchMoviesResponse{
		Movies:       movies,
		TotalResults: result.TotalResults,
	}
}
